#!/bin/bash

clear
MODE="BASIC"

while true
do

echo "$(pwd)"
echo "$(ls -l)"

STARS="**************************************************"
echo ""
echo "$STARS"
echo -e "\t\t Simple file manager"
echo "$STARS"

echo -e "1. List directory contents"
echo -e "2. Change current directory"
echo -e "3. Copy file"
echo -e "4. Move/rename file"
echo -e "5. Remove file"
echo -e "6. Change manager mode"
echo -e "7. Exit"

echo "$STARS"
echo -n "Choice: "

read ENDVAR

case $ENDVAR in
1)	input=""
	echo "$STARS"
	echo "Which directory do you want to list?"
	echo -n "Choice: "
	read input
	echo ""
	if [ $MODE == "BASIC" ]
	then
		ls -1 $input
	else
		ls -l -a $input
	fi
;;
2)	input=""
	echo "$STARS"
	echo -n "Select directory for cd: "
	read input
	cd $input
;;
3)	input=""
	dest=""
	echo "$STARS"
	echo -n "Select file for copy:"
	read input
	echo -n "Select destination folder:"
	read dest
	cp $input $dest
;;
4)	input=""
	dest=""
	echo "$STARS"
	echo -n "Select file for move/rename:"
	read input
	echo -n "Select destination file:"
	read dest
	mv $input $dest
;;
5)	input=""
	echo "$STARS"
	echo -n "Select file for move/rename:"
	read input
	rm $input
;;
6)	input=""
	echo "$STARS"
	echo -n "Enter mode:"
	read input
	if [ $input == "BASIC" ] || [ $input == "FULL" ]
	then
		MODE=$input
	fi
;;
7)	exit 0
esac

done

